pluginManagement {
    repositories {
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
        maven {
            name = "Spring"
            url = uri("https://repo.spring.io/plugins-release/")
        }
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/groups/9994989/-/packages/maven")
        }
    }
}

rootProject.name = "spring-boot-admin"
