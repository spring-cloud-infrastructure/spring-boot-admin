# Spring Boot Admin

Check out [documentation](https://codecentric.github.io/spring-boot-admin/current/) for more info about Codecentric's
Spring Boot Admin.

## 1. Configuration

- [bootstrap.yml](./src/main/resources/bootstrap.yml) (application bootstrap only)
- [config-repo](https://gitlab.com/spring-cloud-infrastructure/config-repo/-/tree/master/config) (externalized
  configuration)

## 2. Build & Run

Check out [docker](https://gitlab.com/spring-cloud-infrastructure/docker) project.
