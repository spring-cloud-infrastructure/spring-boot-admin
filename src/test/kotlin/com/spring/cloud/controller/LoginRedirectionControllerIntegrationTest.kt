package com.spring.cloud.controller

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@WebMvcTest(LoginRedirectionController::class)
internal class LoginRedirectionControllerIntegrationTest(
    @Value("\${services.api-gateway.url}/\${spring.boot.admin.context-path}") private val springBootAdminApiGatewayUrl: String,
    @Autowired private val mockMvc: MockMvc
) {

    @Test
    fun `given request URI has redirect query parameter pointing to spring boot admin API gateway URL when login endpoint is triggered then redirect url from query parameter is returned`() {
        val redirectToUrl = "$springBootAdminApiGatewayUrl/hello-world"

        mockMvc.get("/spring-boot-admin/login?redirectTo=$redirectToUrl") { }
            .andDo { print() }
            .andExpect { redirectedUrl(redirectToUrl) }
    }

    @Test
    fun `given request URI has redirect query parameter not pointing to spring boot admin API gateway URL when login endpoint is triggered then redirect to spring boot admin API gateway URL is returned`() {
        val redirectToUrl = "hello world$springBootAdminApiGatewayUrl"

        mockMvc.get("/spring-boot-admin/login?redirectTo=$redirectToUrl") { }
            .andDo { print() }
            .andExpect {
                status { isFound() }
                redirectedUrl(springBootAdminApiGatewayUrl)
            }
    }

    @Test
    fun `given request URI has no redirect query parameter when login endpoint is triggered then redirect to spring boot admin API gateway URL is returned`() {
        mockMvc.get("/spring-boot-admin/login") { }
            .andDo { print() }
            .andExpect {
                status { isFound() }
                redirectedUrl(springBootAdminApiGatewayUrl)
            }
    }
}
