package com.spring.cloud.notification

import de.codecentric.boot.admin.server.domain.entities.Instance
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository
import de.codecentric.boot.admin.server.domain.events.InstanceEvent
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent
import de.codecentric.boot.admin.server.notify.AbstractEventNotifier
import mu.KotlinLogging.logger
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
internal class LogNotifier(instanceRepository: InstanceRepository) : AbstractEventNotifier(instanceRepository) {

    private val logger = logger { }

    override fun doNotify(event: InstanceEvent, instance: Instance): Mono<Void> = Mono.fromRunnable {
        when (event) {
            is InstanceStatusChangedEvent -> logger.info { "Instance ${instance.registration.name} (${event.getInstance()}) is ${event.statusInfo.status}" }
            else -> logger.info { "Instance ${instance.registration.name} (${event.instance}) {$event.type}" }
        }
    }
}
