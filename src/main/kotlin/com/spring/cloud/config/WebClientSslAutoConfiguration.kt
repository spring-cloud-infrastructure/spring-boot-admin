package com.spring.cloud.config

import com.spring.cloud.config.SslProperties.KeyStoreType
import io.netty.handler.ssl.ClientAuth.REQUIRE
import io.netty.handler.ssl.SslContextBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ClientHttpConnector
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import reactor.netty.http.client.HttpClient
import java.io.File
import java.io.FileInputStream
import java.security.KeyStore
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.TrustManagerFactory

@Configuration
internal class WebClientSslAutoConfiguration {

    @Bean
    fun httpClient(sslProperties: SslProperties): HttpClient = HttpClient.create().secure {
        val sslContextBuilder = SslContextBuilder.forClient()
            .clientAuth(REQUIRE)
            .keyManager(buildKeyManagerFactory(sslProperties))
            .trustManager(buildTrustManagerFactory(sslProperties))
        it.sslContext(sslContextBuilder)
    }

    /**
     * https://github.com/reactor/reactor-netty/issues/640#issuecomment-478738139
     */
    @Bean
    fun clientHttpsConnector(httpClient: HttpClient): ClientHttpConnector = ReactorClientHttpConnector(httpClient)

    private fun buildKeyManagerFactory(sslProperties: SslProperties): KeyManagerFactory =
        KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm()).apply {
            val keyStore = buildKeyStore(
                sslProperties.keyStoreType,
                sslProperties.keyStore,
                sslProperties.keyStorePassword
            )

            init(keyStore, sslProperties.keyStorePassword.toCharArray())
        }

    private fun buildTrustManagerFactory(sslProperties: SslProperties): TrustManagerFactory =
        TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).apply {
            val trustStore = buildKeyStore(
                sslProperties.trustStoreType,
                sslProperties.trustStore,
                sslProperties.trustStorePassword
            )

            init(trustStore)
        }

    private fun buildKeyStore(
        keyStoreType: KeyStoreType,
        keyStoreFile: File,
        keyStorePassword: String,
    ): KeyStore = KeyStore.getInstance(keyStoreType.name).apply {
        FileInputStream(keyStoreFile).use { load(it, keyStorePassword.toCharArray()) }
    }
}
