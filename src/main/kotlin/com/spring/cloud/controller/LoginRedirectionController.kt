package com.spring.cloud.controller

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.StringUtils.startsWithIgnoreCase
import org.apache.http.NameValuePair
import org.apache.http.client.utils.URLEncodedUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus.FOUND
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import java.nio.charset.StandardCharsets.UTF_8
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("\${spring.boot.admin.context-path}")
internal class LoginRedirectionController(
    @Value("\${services.api-gateway.url}/\${spring.boot.admin.context-path}") private val springBootAdminApiGatewayUrl: String,
) {

    companion object {
        private const val REDIRECT_TO_QUERY_PARAMETER = "redirectTo"
        private const val REDIRECT_PREFIX = "redirect:"
    }

    @RequestMapping("login")
    @ResponseStatus(FOUND)
    fun redirectAfterSuccessfullyLogin(request: HttpServletRequest): String =
        "$REDIRECT_PREFIX${buildRedirectUri(request)}"

    private fun buildRedirectUri(request: HttpServletRequest) = request.let(HttpServletRequest::getQueryString)
        .takeIf(StringUtils::isNotEmpty)
        .let { URLEncodedUtils.parse(it, UTF_8) }
        .filter { queryParameter -> REDIRECT_TO_QUERY_PARAMETER == queryParameter.name }
        .map(NameValuePair::getValue)
        .firstOrNull { redirectToUrl -> startsWithIgnoreCase(redirectToUrl, springBootAdminApiGatewayUrl) }
        ?: springBootAdminApiGatewayUrl
}
